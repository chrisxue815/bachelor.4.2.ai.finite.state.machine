﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AIGame
{
    public class Entity
    {
        public AIGame Game { get; set; }

        public Vector2 Position { get; set; }

        public Texture2D Texture { get; set; }

        public Entity()
        {
            Game = AIGame.Instance;
        }

        public virtual void Initialize()
        {
        }

        public virtual void LoadContent()
        {
        }

        public virtual void Update(float deltaSeconds)
        {
        }

        public virtual void Draw(float deltaSeconds)
        {

        }

        /// <summary>
        /// This function takes a Vector2 as input, and returns that vector "clamped"
        /// to the current Graphics viewport. We use this function to make sure that 
        /// no one can go off of the screen.
        /// </summary>
        protected void ClampPositionToViewport()
        {
            var viewport = Game.Graphics.GraphicsDevice.Viewport;
            var x = MathHelper.Clamp(Position.X, viewport.X, viewport.X + viewport.Width);
            var y = MathHelper.Clamp(Position.Y, viewport.Y, viewport.Y + viewport.Height);
            Position = new Vector2(x, y);
        }
    }
}
